# StarBall24

You can access the project proposal here: [Project Proposal](https://gitlab.com/handalkhom/gemelli-gemini/-/blob/main/Proposal_StarBall24.pdf)

You can access the project repositories: [Project GitHub](https://github.com/orgs/StarBall24/repositories)

Youtube Video Presentation : [YouTube](https://youtu.be/IgI3Vvt2e20)

Final Report of the Project: [Final Report](https://gitlab.com/handalkhom/gemelli-gemini/-/blob/main/StarBall24_Final_Report.pdf)

## Description

StarBall24 is a cutting-edge mobile application designed for football enthusiasts and fans. It provides real-time match predictions, team standings, and fixture results for various football leagues worldwide. Users can stay informed, engage with their favorite teams, and make informed decisions based on data-driven insights.

## Topic and Motivation

Football can train the body and make it stronger. Football also requires persistence in order to achieve the goal of playing football. Therefore, in order to support the enthusiasm of football fans to always play, support and enjoy football, StarBall was created to support this.

## Target User Group

- Football Lovers
- Football Observers
- Actors in the Football Industry

## Proposed Solution
### Key Features

- Match Predictions
- Team Standings
- Results and Fixtures

### Alignment with SDGs Goals

SDG Goal 3: Good Health and Well-Being. Here’s how StarBall24 contributes: 

By providing accurate match predictions, StarBall24 enhances fans’ emotional wellbeing. Reduces stress related to uncertainty by offering data-backed insights. Encourages a healthier approach to sports engagement.

SDG Goal 9: Industry, Innovation, and Infrastructure. Here’s how StarBall24 contributes:

StarBall24 leverages innovative machine learning techniques for match predictions. Contributes to the development of digital infrastructure in the sports industry. Fosters collaboration with football clubs, data providers, and fans.

## Technologies

- Figma
- Random Forest Classification
- Flask
- Python
- Kotlin
- Google Cloud Run
- TensorFlow

## References

[You can see references here](https://gitlab.com/handalkhom/gemelli-gemini/-/blob/main/Proposal_StarBall24.pdf)

## Meet Us

- [Handal Khomsyat - 1217050061](https://github.com/handalkhom/)
- [Fazar Budiman - 1217050056](https://github.com/FazarBudiman)
- [M Dzafadhlan F Muzakki - 1217050075](https://github.com/FadhlanDza1)
- [M Hasbi Hasbullah - 1217050076](https://github.com/mhmdhasbi)